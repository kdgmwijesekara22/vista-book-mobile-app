import React from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {Provider} from "react-redux";
import {StatusBar} from 'expo-status-bar';
import { useFonts } from 'expo-font';
import { NavigationContainer } from '@react-navigation/native';
import store from './store';
import MainNavigator from "./navigation/MainNavigator";

// custom fonts
const fontsData = {
    'Nunito': require('../../vista-book-mobile-app/src/assets/Fonts/Inter-Regular.ttf'),
    'Nunito-light': require('../../vista-book-mobile-app/src/assets/Fonts/Inter-Light.ttf'),
    'Nunito-SemiBold': require('../../vista-book-mobile-app/src/assets/Fonts/Inter-Medium.ttf'),
    'Nunito-Bold': require('../../vista-book-mobile-app/src/assets/Fonts/Inter-SemiBold.ttf'),
};

export default function App() {
    const [loaded] = useFonts(fontsData);
    if (!loaded) {
        console.log('not loaded')
    }
    return (
        <Provider store={store}>
            <StatusBar/>
            <NavigationContainer>
                <MainNavigator />
            </NavigationContainer>
        </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
