import { createStackNavigator } from 'react-navigation-stack';
import StartScreen from '../screens/StartScreen';
import Login from '../screens/LoginScreen';
import SignUp from '../screens/RegisterScreen';
import ResetPasswordScreen from '../screens/ResetPasswordScreen';

const AuthStackNavigatorConfig = {
    StartScreen: {
        screen: StartScreen,
    },
    Login: {
        screen: Login,
    },
    SignUp: {
        screen: SignUp,
    },
    ResetPassword: {
        screen: ResetPasswordScreen,
    },
};

const AuthStackNavigator = createStackNavigator(AuthStackNavigatorConfig, {
    initialRouteName: 'StartScreen',
    headerMode: 'none',
});

export default AuthStackNavigator;
