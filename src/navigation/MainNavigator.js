import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import AuthStackNavigator from './AuthStackNavigator';
import AppTabNavigation from './index';  // This refers to the previously named books.js file

const MainNavigator = createSwitchNavigator({
    Auth: AuthStackNavigator,
    App: AppTabNavigation,
}, {
    initialRouteName: 'Auth'
});

export default createAppContainer(MainNavigator);
