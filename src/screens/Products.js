import React, {useEffect} from 'react';
import { StyleSheet, View } from 'react-native';
import CategoriesBlock from '../components/Block/Categories';
import HorizontalProductView from '../components/Block/HorizontalProductView';
import UiText from '../components/UI/Text';
import UiView from '../components/UI/View';
import COLORS from '../constants/COLORS';
import { useSelector } from 'react-redux';
import OfferBlock from '../components/Block/Offer';
import UiAlert from '../components/UI/Alert';
// import   PopularProducts  from '../dummy_data/products';
import * as api from '../api/index';

const Products = ({ navigation }) => {
  let popularProducts = useSelector((state) => state.product.books);

/*  useEffect(() => {  // 2. Set up useEffect
    console.log("Fetching all books...");  // Debugging log
    const fetchAllBooks = async () => {
      try {
        const response = await api.getAllBooks();
        console.log("Fetched books:", response.data);  // Debugging log
        popularProducts = response.data;
        // dispatch({ type: 'FETCH_ALL', payload: response.data });  // Dispatch the result to store
      } catch (error) {
        console.error("Error fetching books:", error);  // Debugging log
      }
    };

    fetchAllBooks();
  }, []);*/
  return (
    <View style={styles.view}>
      <UiView style={styles.screen}>
        <View style={styles.head}>
          <UiText style={styles.heading}>Vista Books</UiText>
          <UiText style={styles.subHeading}>A perfect readers stop</UiText>
        </View>
        <CategoriesBlock navigation={navigation} />
        <HorizontalProductView
          data={popularProducts}
          navigation={navigation}
          headTitle="Popular Books"
        />
        <OfferBlock />
        <View style={styles.empty}></View>
      </UiView>
      <UiAlert />
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    position: 'relative',
    flex: 1,
  },
  screen: {
    paddingTop: 50,
  },
  head: {
    paddingHorizontal: 15,
  },
  heading: {
    fontSize: 20,
    fontFamily: 'Nunito-Bold',
    color: COLORS.primaryColor,
  },
  subHeading: {
    fontSize: 16,
    color: COLORS.textColorLight,
  },
  empty: {
    minHeight: 60,
  },
});

export default Products;
