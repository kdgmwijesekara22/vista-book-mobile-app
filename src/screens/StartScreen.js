import React from 'react'
import Background from '../components/Background'
import Logo from '../components/Logo'
import Header from '../components/Header'
import Button from '../components/Button'
import Paragraph from '../components/Paragraph'
import {useDispatch} from "react-redux";
import {getBooks} from "../store/actions/books";

export default function StartScreen({ navigation }) {

    const dispatch = useDispatch();  // Initialize the dispatch hook

    return (
    <Background>
      <Logo />
      <Header>Vista Books</Header>
      <Paragraph>
        Welcome to online book store.
      </Paragraph>
      <Button
        mode="contained"
        onPress={() => {
            navigation.navigate('Products')}
        }
      >
        Login
      </Button>
      <Button
        mode="outlined"
        onPress={() => navigation.navigate('RegisterScreen')}
      >
        Sign Up
      </Button>
    </Background>
  )
}
