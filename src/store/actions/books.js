//books actions class
import {
    FETCH_BY_SEARCH,
    SET_BOOKS,
    SELECT_BOOK,
    FETCH_ALL,
    FETCH_BOOK_BY_ID,
    CREATE_BOOK,
    UPDATE_BOOK,
    DELETE_BOOK,
    CATEGORY_FICTION,
    CATEGORY_NONFICTION,
    CATEGORY_KIDS,
    CATEGORY_SCIENCE_TECHNOLOGY,
    CATEGORY_GRAPHIC_NOVELS_COMICS,
    CATEGORY_POETRY,
    SUBMIT_REVIEW_FAILURE,
    SUBMIT_REVIEW_SUCCESS,
    SUBMIT_REVIEW_REQUEST
} from '../../variables/constants/actionTypes';
import * as api from '../../api';
import {submitReviewApiCall} from "../../api";
import * as ReactNativeToast from "react-native-toast-message/lib/src/Toast";

export const submitReview = (reviewObject) => async (dispatch) => {
    dispatch({ type: SUBMIT_REVIEW_REQUEST });

    try {
        const response = await submitReviewApiCall(reviewObject);
        dispatch({
            type: SUBMIT_REVIEW_SUCCESS,
            payload: response.data
        });
    } catch (error) {
        dispatch({
            type: SUBMIT_REVIEW_FAILURE,
            payload: error.message
        });
    }
};

export const getBooks  = () => async (dispatch) => {
    try {
        const { data } = await api.getAllBooks();
        ReactNativeToast.show('Successfully fetched books');
        dispatch({ type: FETCH_ALL, payload: data });
    } catch (error) {
        console.log(error.message);
        ReactNativeToast.show(`Error: ${error.message}`);
    }
}

export const getBookByCategory = (category) => async(dispatch) => {
    try {
        let categoryConstant = '';
        switch (category) {
            case 'Fiction':
                categoryConstant = CATEGORY_FICTION;
                break;
            case 'Nonfiction':
                categoryConstant = CATEGORY_NONFICTION;
                break;
            case 'Kids':
                categoryConstant = CATEGORY_KIDS;
                break;
            case 'Science & Technology':
                categoryConstant = CATEGORY_SCIENCE_TECHNOLOGY;
                break;
            case 'Graphic Novels & Comics':
                categoryConstant = CATEGORY_GRAPHIC_NOVELS_COMICS;
                break;
            case 'Poetry':
                categoryConstant = CATEGORY_POETRY;
                break;
            default:
                ReactNativeToast.show(`Unknown category: ${category}`);

        }

        const { data } = await api.getBooksByCategory(categoryConstant);
        toast.success('Successfully fetched books by category');
        dispatch({ type: FETCH_ALL, payload: data });
    } catch (error) {
        console.log(error.message);
        toast.error(`Error: ${error.message}`);
    }
}

export const getBook = (id) => async (dispatch) => {
    try {
        const { data } = await api.getBookByIsbn(id);
        toast.success('Successfully fetched the book');
        dispatch({ type: FETCH_BOOK_BY_ID, payload: data });
    } catch (error) {
        console.log(error.message);
        toast.error(`Failed to fetch the book: ${error.message}`);
    }
}

export const createBook = (book, bookCoverFile, eBookFile) => async (dispatch) => {
    try {
        const { data } = await api.createBook(book, bookCoverFile[0], eBookFile[0]);
        toast.success('Book created successfully');
        dispatch({ type: CREATE_BOOK, payload: data });
    }catch (error) {
        console.log(error.message);
        toast.error(`Failed to create the book: ${error.message}`);
    }
}

export const updateBook = (id, book) => async (dispatch) => {
    try {
        const { data } = await api.updateBook(book);
        toast.success('Book updated successfully');
        dispatch({ type: UPDATE_BOOK, payload: data });
    } catch (error) {
        console.log(error.message);
        toast.error(`Failed to update the book: ${error.message}`);
    }
}

export const deleteBook = (id) => async (dispatch) => {
    try {
        await api.deleteBook(id);
        toast.success('Book deleted successfully');
        dispatch({ type: DELETE_BOOK, payload: id });
    } catch (error) {
        console.log(error.message);
        toast.error(`Failed to delete the book: ${error.message}`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------
export const setBooks = (books) => {
    console.log("books setbook",books)
    return {
        type: SET_BOOKS,
        payload: books,
    };
};

export const selectBook = (book) => {
    return {
        type: SELECT_BOOK,
        payload: book
    };
};

export const getBookById = (id) => async (dispatch) => {
    try {
        const post = dummyData.find((book) => book._id === id);

        dispatch({
            type: FETCH_BOOK_BY_ID,
            payload: { post },
        });
    } catch (error) {
        console.log(error);
    }
};

export const getBooksBySearch = (searchQuery) => async (dispatch, getState) => {
    try {
        const { books } = getState().books; // Assuming you have a books state in your Redux store

        // Filter or modify the books array based on the searchQuery if needed
        dispatch({ type: FETCH_BY_SEARCH, payload: { data: books } });
    } catch (error) {
        console.log(error);
    }
};

