import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';  // Adjust this path to point to your combined reducers

const store = createStore(
    rootReducer,
    applyMiddleware(thunk)
);



export default store;
