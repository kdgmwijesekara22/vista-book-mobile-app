import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {NavigationContainer} from "@react-navigation/native";
import Test from "./components/user/Home/Test";

const Stack = createNativeStackNavigator();

function Routes() {
  return (
      <NavigationContainer>
          <Stack.Navigator initialRouteName="Test">
              <Stack.Screen name="Test" component={Test} />

              {/*<Stack.Screen name="Book" component={Book} />*/}
              {/*<Stack.Screen name="BookDetails" component={BookDetails} />*/}
          </Stack.Navigator>

      </NavigationContainer>

  );
}

export default Routes;
